package com.legacy.leap.player;

import java.util.function.Consumer;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.leap.LeapRegistry;
import com.legacy.leap.player.util.ILeapPlayer;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.util.NonNullSupplier;
import net.minecraftforge.event.entity.living.LivingFallEvent;

public class LeapPlayer implements ILeapPlayer
{
	@CapabilityInject(ILeapPlayer.class)
	public static Capability<ILeapPlayer> INSTANCE = null;

	private PlayerEntity player;
	private boolean doubleJumped = false;

	public LeapPlayer()
	{
	}

	public LeapPlayer(PlayerEntity player)
	{
		super();
		this.player = player;
	}

	@Nullable
	public static ILeapPlayer get(PlayerEntity player)
	{
		return LeapPlayer.getIfPresent(player, (skyPlayer) -> skyPlayer);
	}

	public static <E extends PlayerEntity> void ifPresent(E player, Consumer<ILeapPlayer> action)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			action.accept(player.getCapability(INSTANCE).resolve().get());
	}

	@Nullable
	public static <E extends PlayerEntity, R> R getIfPresent(E player, Function<ILeapPlayer, R> action)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			return action.apply(player.getCapability(INSTANCE).resolve().get());
		return null;
	}

	public static <E extends PlayerEntity, R> R getIfPresent(E player, Function<ILeapPlayer, R> action, NonNullSupplier<R> elseSupplier)
	{
		if (player != null && player.getCapability(INSTANCE).isPresent())
			return action.apply(player.getCapability(INSTANCE).resolve().get());
		return elseSupplier.get();
	}

	@Override
	public CompoundNBT writeAdditional(CompoundNBT compound)
	{
		compound.putBoolean("UsedDoubleJump", this.hasDoubleJumped());

		return compound;
	}

	@Override
	public void read(CompoundNBT compound)
	{
		this.setDoubleJumping(compound.getBoolean("UsedDoubleJump"));
	}

	@Override
	public PlayerEntity getPlayer()
	{
		return this.player;
	}

	@Override
	public boolean hasDoubleJumped()
	{
		return this.doubleJumped;
	}

	@Override
	public void setDoubleJumping(boolean used)
	{
		if (used)
			this.onDoubleJumped();
		else
			this.doubleJumped = false;
	}

	public void onDoubleJumped()
	{
		PlayerEntity entity = this.getPlayer();
		float enchantmentLevel = EnchantmentHelper.getMaxEnchantmentLevel(LeapRegistry.LEAPING, entity);
		boolean canJump = !entity.isOnGround() && !this.hasDoubleJumped();

		if (enchantmentLevel <= 0)
			return;

		// if the player can jump, jump
		if (canJump && !entity.isElytraFlying() && !entity.abilities.isFlying)
		{
			entity.setMotion(entity.getMotion().getX(), 0.6D, entity.getMotion().getZ());
			entity.velocityChanged = true;

			entity.world.playSound(null, entity.getPosition(), SoundEvents.ENTITY_ENDER_DRAGON_FLAP, SoundCategory.PLAYERS, 0.3F, 2.0F);

			if (entity.world instanceof ServerWorld)
			{
				for (int i = 0; i < 20; ++i)
				{
					double d0 = entity.world.rand.nextGaussian() * 0.02D;
					double d1 = entity.world.rand.nextGaussian() * 0.02D;
					double d2 = entity.world.rand.nextGaussian() * 0.02D;

					((ServerWorld) entity.world).spawnParticle(ParticleTypes.POOF, entity.getPosX() + (double) (entity.world.rand.nextFloat() * entity.getWidth() * 2.0F) - (double) entity.getWidth() - d0 * 10.0D, entity.getPosY() - d1 * 10.0D, entity.getPosZ() + (double) (entity.world.rand.nextFloat() * entity.getWidth() * 2.0F) - (double) entity.getWidth() - d2 * 10.0D, 1, d0, d1, d2, 0.0F);
				}
			}

			this.doubleJumped = true;
		}
	}

	@Override
	public void onFall(LivingFallEvent event)
	{
		float enchantmentLevel = EnchantmentHelper.getMaxEnchantmentLevel(LeapRegistry.LEAPING, this.getPlayer());

		if (this.hasDoubleJumped())
		{
			if (enchantmentLevel > 0)
			{
				int i = MathHelper.ceil((event.getDistance() - 3.0F - enchantmentLevel) * event.getDamageMultiplier());
				event.setDistance(i);
			}

			this.setDoubleJumping(false);
		}
	}
}
