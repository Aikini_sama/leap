package com.legacy.leap.player.util;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraftforge.event.entity.living.LivingFallEvent;

public interface ILeapPlayer
{
	CompoundNBT writeAdditional(CompoundNBT nbt);

	void read(CompoundNBT nbt);

	boolean hasDoubleJumped();

	void setDoubleJumping(boolean used);

	PlayerEntity getPlayer();

	void onFall(LivingFallEvent event);
}