package com.legacy.leap.network.c_to_s;

import java.util.function.Supplier;

import com.legacy.leap.player.LeapPlayer;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

public class DoubleJumpPacket
{
	public DoubleJumpPacket()
	{
	}

	public static void encoder(DoubleJumpPacket packet, PacketBuffer buff)
	{
	}

	public static DoubleJumpPacket decoder(PacketBuffer buff)
	{
		return new DoubleJumpPacket();
	}

	public static void handler(DoubleJumpPacket packet, Supplier<NetworkEvent.Context> context)
	{
		context.get().enqueueWork(() -> handlePacket(packet, context.get().getSender()));
		context.get().setPacketHandled(true);
	}

	private static void handlePacket(DoubleJumpPacket packet, PlayerEntity player)
	{
		LeapPlayer.ifPresent(player, (leapPlayer) ->
		{
			leapPlayer.setDoubleJumping(true);
		});
	}
}
