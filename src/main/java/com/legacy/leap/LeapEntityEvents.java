package com.legacy.leap;

import com.legacy.leap.player.LeapPlayer;
import com.legacy.leap.player.util.CapabilityProvider;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class LeapEntityEvents
{
	@SubscribeEvent
	public void onCapabilityAttached(AttachCapabilitiesEvent<Entity> event)
	{
		if (event.getObject() instanceof PlayerEntity && !event.getObject().getCapability(LeapPlayer.INSTANCE).isPresent())
			event.addCapability(LeapMod.locate("player_capability"), new CapabilityProvider(new LeapPlayer((PlayerEntity) event.getObject())));
	}

	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent event)
	{
	}

	@SubscribeEvent
	public void onLivingFall(LivingFallEvent event)
	{
		if (event.getEntityLiving() instanceof PlayerEntity && !event.getEntityLiving().world.isRemote)
			LeapPlayer.ifPresent((PlayerEntity) event.getEntity(), leapPlayer -> leapPlayer.onFall(event));
	}
}
