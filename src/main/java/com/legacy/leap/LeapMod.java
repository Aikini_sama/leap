package com.legacy.leap;

import com.legacy.leap.client.LeapClientEvents;
import com.legacy.leap.network.PacketHandler;
import com.legacy.leap.player.LeapPlayer;
import com.legacy.leap.player.util.CapabilityStorage;
import com.legacy.leap.player.util.ILeapPlayer;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(LeapMod.MODID)
public class LeapMod
{
	public static final String NAME = "Leap";
	public static final String MODID = "leap";

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public LeapMod()
	{
		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
		{
			FMLJavaModLoadingContext.get().getModEventBus().addListener(Client::clientInit);
			MinecraftForge.EVENT_BUS.register(new LeapClientEvents());
		});

		FMLJavaModLoadingContext.get().getModEventBus().addListener(LeapMod::commonInit);
		MinecraftForge.EVENT_BUS.register(new LeapEntityEvents());
	}

	public static void commonInit(final FMLCommonSetupEvent event)
	{
		PacketHandler.register();
		CapabilityManager.INSTANCE.register(ILeapPlayer.class, new CapabilityStorage(), LeapPlayer::new);
	}

	@OnlyIn(Dist.CLIENT)
	public static class Client
	{
		@OnlyIn(Dist.CLIENT)
		public static net.minecraft.client.settings.KeyBinding LEAP_KEYBIND = new net.minecraft.client.settings.KeyBinding("key.leap.double_jump", 32, "key.categories.movement");

		@SubscribeEvent
		public static void clientInit(FMLClientSetupEvent event)
		{
			LeapKeyRegistry leapKeyRegistry = new LeapKeyRegistry();
			MinecraftForge.EVENT_BUS.register(leapKeyRegistry);
			leapKeyRegistry.register(LEAP_KEYBIND);
		}
	}
}
